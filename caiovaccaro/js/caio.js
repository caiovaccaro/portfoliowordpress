jQuery(function($){
	var CAIO = {};

	function get(name){
	   if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
	      return decodeURIComponent(name[1]);
	}

	CAIO.Start = function() {
		if(!$('body').hasClass('wp-admin')) {
			for(var obj in CAIO) {
				for(var method in CAIO[obj]) {
					if(method.indexOf('_') !== 0 && typeof CAIO[obj][method] === 'function') CAIO[obj][method]();
				}
			}
		}
	};

	CAIO.Facebook = {
		init: function() {
			window.fbAsyncInit = function() {
			    
			    FB.init({
			    	appId:'534453986601504',
			    	cookie:true,
			    	status:true,
			    	xfbml:true
			    });

			}
		}
	};

	CAIO.Projects = {
		packery: function() {
			if($('#projects').length) {
				var container = $('#projects');

				container.packery({
				  itemSelector: '.project',
				  gutter: 15
				});
			}
		}
	};

	CAIO.Common = {
		sidebar: function() {

			var cookie_name = 'cvftycm',
				between_animations = false,
				first_trigger = true;

			function setSideBarHeight() {
				var height = $(window).height() < $('#main').height() ? $('#main').height() : $(window).height();
				$('#site-navigation, #left, #site-navigation-mouse').css({ 'height': height });
			}

			setSideBarHeight();

			$(window).resize(function() {
				setSideBarHeight();
			}).load(function() {
				setSideBarHeight();
			});

			if(!$('html').hasClass('touch')) {

				if(typeof $.cookie(cookie_name) === 'undefined') {

					$('#site-navigation').show();

					setTimeout(function() {

						between_animations = true;

						$('#site-navigation').animate({'left': '-198px'}, 'swing', function() {
							$(this).css({'border-color': 'black'}).animate({'border-right-width': '9px'}, function() {
								$(this).animate({'border-right-width': '4px'}, function() {
									$('#site-navigation-mouse').addClass('active');
									$.cookie(cookie_name, 'true');
									between_animations = false;
								});
							});
						});

					},2000);

				} else {

					$('#site-navigation').css({'left': '-198px', 'border-color': 'black', 'border-right-width': '4px'});
					$('#site-navigation').show();
					$('#site-navigation-mouse').addClass('provisory').bind('mousemoveoutside', function() {
						setTimeout(function() {
							$('#site-navigation-mouse').addClass('active').removeClass('provisory').unbind('mousemoveoutside');
						},500);
					});

				}

				$('#site-navigation-mouse').mouseenter(function(e) {

					e.stopPropagation();

					if(!$(this).hasClass('active') || between_animations === true) return false;

					between_animations = true;
					first_trigger = false;

					$('#site-navigation').animate({'border-right-width': '1px'}, 100, function() {
						$(this).css({'border-color': '#CCC'}).animate({'left': '0'}, 300, 'swing', function() {
							$('#site-navigation-mouse').removeClass('active');
							between_animations = false;
						});
					});

				});

				$('#site-navigation').mouseleave(function(e) {

					e.stopPropagation();

					if($('#site-navigation-mouse').hasClass('active') || between_animations === true || first_trigger === true) return false;

					between_animations = true;

					$('#site-navigation').animate({'left': '-198px'}, 'swing', function() {
						$(this).css({'border-color': 'black'}).animate({'border-right-width': '9px'}, function() {
							$(this).animate({'border-right-width': '4px'}, function() {
								$('#site-navigation-mouse').addClass('active');
								between_animations = false;
							});
						});
					});
				});

				$('#site-navigation').bind('mousemoveoutside', function(e) {

					e.stopPropagation();

					if($('#site-navigation').is(':hidden') || $('#site-navigation-mouse').hasClass('active') || between_animations === true || first_trigger === true) return false;

					between_animations = true;

					$('#site-navigation').animate({'left': '-198px'}, 'swing', function() {
						$(this).css({'border-color': 'black'}).animate({'border-right-width': '9px'}, function() {
							$(this).animate({'border-right-width': '4px'}, function() {
								$('#site-navigation-mouse').addClass('active');
								between_animations = false;
							});
						});
					});
				});

			} else {

				$('#site-navigation').show();

			}
		}
	};

	CAIO.Contact = {
		autogrow: function() {
			if($('#contact').length) {
				// $('#name').autoGrowInput({
				//     comfortZone: 50,
				//     minWidth: 152,
				//     maxWidth: 200
				// });

				// $('#email').autoGrowInput({
				//     comfortZone: 50,
				//     minWidth: 300,
				//     maxWidth: 530
				// });

				$('#message').focus(function() {
					if($(this).val() == '') {
						$(this).animate({'height': '255px'});
					}
				});

				$('#message').blur(function() {
					if($(this).val() == '') {
						$(this).animate({'height': '33px'});
					}
				});
			}
		},
		validation: function() {

			var submit_form = function() {

				$('#enviar').find('span').fadeOut(function() {
					$('#truck').addClass('notext').animate({'left': '110px'}, 700, 'swing', function() {
						$(this).fadeOut(function() {
							$('#enviar').find('span').text('enviando').addClass('enviando').fadeIn();

							$.ajax({
							  url: $('#logo').find('a').attr('href') + 'contato.php',
							  type: 'POST',
							  data: {name: $('#name').val(), email: $('#email').val(), subject: $('#subject').val(), message: $('#message').val() },
							  success: function(data) {
							    var response = data.split('}'),
							    	status = response[0].replace('{status: ', ''),
							    	message = response[1];

							    if(status == 0) {
							    	// erro
							    	$('.bottom .validation p').text(message).parent().show();
							    	clear_form();

							    } else {
							    	// sucesso
							    	$('.bottom .thanks p').text(message).parent().show();
							    	clear_form();

							    }
							  }
							});
						});
					});
				});
			},
			clear_form = function() {

				$('#name, #email, #subject, #message').val('');
				$('#message').animate({'height': '33px'});

				$('#enviar').find('span').text('enviar').removeClass('enviando').show();
				$('#truck').css({'left': '0'}).removeClass('notext').show();

			};

			if($('#contact').length) {

				$('input, textarea').focus(function() {
					$(this).removeClass('ops');
					$('.thanks, .validation').hide();
					$('.validation p').text('Ops! Esqueceu alguma coisa?');
				});

				$('#enviar').click(function(e) {
					e.preventDefault();

					var fail = false;

					if($('#name').val() === '') {
						$('#name').addClass('ops');
						fail = true;
					}

					if($('#email').val() === '') {
						$('#email').addClass('ops');
						fail = true;
					}

					if($('#subject').val() === '') {
						$('#subject').addClass('ops');
						fail = true;
					}

					if($('#message').val() === '') {
						$('#message').addClass('ops');
						fail = true;
					}

					if(fail === true) {

						$('.validation').show();

					} else {

						$('.validation').hide();

						submit_form();
						
					}

					fail = false;

				});
			}
		}
	};

	
	CAIO.Start();

});