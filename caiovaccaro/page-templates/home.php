<?php
/**
 * Template Name: Home
 */

get_header(); ?>
	<div id="home" class="content">
		<div id="projects">
			<?php
				$projects = new WP_Query(array('post_type' => 'projetos'));

				if($projects->have_posts()) :
					while($projects->have_posts()) : $projects->the_post();
						$template = get_post_meta( get_the_ID(), 'projetos_template', true );
						$cover = kd_mfi_get_featured_image_url( 'projetos_capa', 'projetos' );
						?>
							<div class="project <? if($template == 'w1') { echo 'w1'; } else { echo 'w2'; } ?>">
								<div class="cover" style="background-image: url(<?= $cover ?>);">
									<a href="<?= get_permalink() ?>"></a>
								</div>
								<div class="info">
									<a href="<?= get_permalink() ?>">
										<h2 class="title"><? the_title() ?></h2>
										<p>
											<?= substr(strip_tags(get_the_content()), 0,231); if(strlen(strip_tags(get_the_content())) > 231) { echo '&hellip;'; } ?>
										</p>
									</a>
								</div>
							</div>
						<?php
					endwhile;
				endif;
			?>
		</div>
	</div>
<?php get_footer(); ?>