<?php
 
function register_my_menus() {
	register_nav_menus(
		array(
			'categories' => __( 'Categorias' )
		)
	);
}
add_action( 'init', 'register_my_menus' );