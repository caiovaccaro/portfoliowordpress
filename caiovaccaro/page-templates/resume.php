<?php
/**
 * Template Name: Resume
 */

get_header(); ?>
	<div id="resume" class="content">
		<div class="top">
			<div class="img"></div>
			<div class="text">
				<p>Caio Vaccaro</p>
				<p>Idade: 24 anos</p>
				<p>Ocupação: Programador Web</p>
				<p>Rio de Janeiro, Brasil</p>
				<a href="https://www.facebook.com/caiovaccaro" target="_blank">
					<i class="ss-icon ss-social-regular">facebook</i>
				</a>
				<a href="http://br.linkedin.com/pub/caio-vaccaro/6a/786/86b" target="_blank">
					<i class="ss-icon ss-social-regular ss-linkedin"></i>
				</a>
			</div>
		</div>
		<div class="middle">
			<p>O que me trouxe à programação foi meu interesse por design. Conseguir transformar um layout, uma ideia, em algo funcional foi como comecei. Passando por uma variedade de tecnologias, hoje me aprofundo em desenvolvimento de sistemas (backend) e mobile.</p>
			<h2>Objetivo</h2>
			<p>Atuar como programador front-end e back-end.</p>
			<h2>Experiência profissional</h2>
			<p>Seis anos de experiência profissional.<br><br>
			Em 2006 participação como Web Designer na agência Estudios-Aqua.<br><br>
			De 2012 a 2013, programador na Niramekko.<br><br>
			<a href="http://www.niramekko.com.br/" target="_blank">niramekko.com.br</a><br>
			<a href="http://poe.ma" target="_blank">poe.ma</a><br>
			<a href="http://plau.co" target="_blank">plau.co</a>
			</p>
			<h2>Tecnologias</h2>
			<p>XHTML, HTML5, CSS3, Javascript, jQuery, AJAX, JSON, XML, PHP, MySQL, Node.js, Wordpress, Magento, LESS, SASS, SEO e Facebook API.</p>
			<h2>Educação</h2>
			<p>Curso técnico (3 anos) em Comunicação Social e Informática pelo Instituto ORT (aulas como Linguagem e Mídia, História da Arte, Programação e Design Gráfico).<br><br>
				Bacharel incompleto em Psicologia (2 anos e meio).<br><br>
				Formações em Arquitetura e Planejamento de Sites, XHTML, CSS, HTML5, PHP e MySQL pelo ILearn. <a href="http://ilearn.com.br">ilearn.com.br</a></p>
			<h2>Idiomas</h2>
			<p>Português e inglês</p>
		</div>
	</div>
<?php get_footer(); ?>