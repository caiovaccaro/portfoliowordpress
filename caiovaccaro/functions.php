<?php

define('FILE_PATH', plugin_basename( __FILE__ ));

function load_custom_wp_admin_style() {
        wp_register_style( 'custom_wp_admin_css',  get_bloginfo( 'stylesheet_directory' ) . '/custom-admin-style.css', false, '1.0.0' );
        wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

function custom_admin_js() {
	wp_enqueue_script(
		'handlebars',
		get_bloginfo('stylesheet_directory') . '/components/lib/handlebars.js'
	);
	wp_enqueue_script(
		'chosen',
		get_bloginfo('stylesheet_directory') . '/components/chosen/chosen.jquery.min.js',
		array('jquery')
	);
	wp_enqueue_script(
		'custom-admin',
		get_bloginfo('stylesheet_directory') . '/js/caio-admin.js',
		array('chosen')
	);
}    
add_action('admin_enqueue_scripts', 'custom_admin_js');

function custom_js() {
	wp_enqueue_script(
		'libs',
		get_bloginfo('stylesheet_directory') . '/js/lib.min.js',
		array('jquery'),
		'1.0',
		true
	);
	wp_enqueue_script(
		'caio',
		get_bloginfo('stylesheet_directory') . '/js/caio.min.js',
		array('jquery'),
		'1.0',
		true
	);
}    
add_action('wp_enqueue_scripts', 'custom_js');

function custom_rewind_loop($loop) {
	$loop->current_post = -1;
	if ( $loop->post_count > 0 ) {
		$loop->post = $loop->posts[0];
	}
}


// Utilities
require_once("components/utilities/getters.php");
require_once("components/utilities/checks.php");

// Classes
require_once("components/classes/Form.php");


// Post Types
require_once("components/post_types/register.php");

require_once("components/post_types/projetos/taxonomies.php");
require_once("components/post_types/projetos/meta_boxes.php");
require_once("components/post_types/projetos/save.php");

require_once("components/menus/register.php");