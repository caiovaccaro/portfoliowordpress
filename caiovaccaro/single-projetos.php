<?php get_header(); ?>
	<div id="projeto" class="content">
		<?php
			if(have_posts()) : the_post();
				$category = join_taxonomies('projetos', 'categorias', $post->ID, null, null);
				$lines_of_code = get_post_meta( get_the_ID(), 'projetos_lines_of_code', true );
				$credits = get_post_meta( get_the_ID(), 'projetos_credits', true );
				$featured_img = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'full' );
				?>
					<div id="header">
						<h2><?= the_title() ?></h2>
						<div class="data"><?php if($category) { echo ' • '.$category; } ?><?php if($lines_of_code) { echo ' • linhas de código: '.$lines_of_code; } ?></div>
						<div class="social">
							<div class="facebook">
								<fb:like href="<?= get_permalink() ?>" layout="button_count" width="450" show_faces="false" font="trebuchet ms"></fb:like>
							</div>
						</div>
					</div>
					<div id="featured_img">
						<img src="<?= $featured_img ?>" alt="<?= the_title() ?>">
					</div>
					<div id="projeto_content">
						<?php the_content() ?>
						<?php
							if($credits) :
								?>
									<div class="line"></div>
									<b>Créditos e agradecimentos:</b>
									<?= $credits ?>
								<?php
							endif;
						?>
					</div>
				<?php
			endif;
		?>
	</div>
<?php get_footer(); ?>