<?php
function get_custom_related_field($post_type, $id) {
	$args = array( 'post_type' => $post_type, 'nopaging' => true );
	$loop = new WP_Query( $args );

	if($loop->have_posts()) : while($loop->have_posts()) : $loop->the_post();
		$title = get_the_title();
		$value = str_replace('-','_',(str_replace('?'.$post_type.'=','',(basename(get_permalink())))));

		if($value == $id) {
			return $title;
		}

	endwhile; wp_reset_postdata();  endif;
}

function get_ID_by_slug($page_slug) {
    $page = get_page_by_path($page_slug);
    if ($page) {
        return $page->ID;
    } else {
        return null;
    }
}

function join_taxonomies($post_type, $tax, $id, $base_url, $link = 'link') {

	$taxonomy = get_the_terms($id, $post_type.'_'.$tax);
	$post_type_id = get_ID_by_slug($post_type);
	$base_post_type_url = get_permalink($post_type_id);

	if(!$base_url) {
		$path = $base_post_type_url;
	} else {
		$path = $base_url;
	}

	if($taxonomy && ! is_wp_error( $taxonomy )) {
		$taxonomies = array();

		foreach ( $taxonomy as $term ) {
			$url = add_query_arg(array('taxonomia'=>$post_type.'_'.$tax, 'tv'=>$term->slug), $path);
			if($link == 'link') {
				$atag = '<a href="'.$url.'">';
				$aclose = '</a>';
			}
			$taxonomies[] = $atag.$term->name.$aclose;
		}
							
		$taxonomy = join( ', ', $taxonomies );
	}

	return $taxonomy;
}

?>