<?php
/**
 * Template Name: Artigos
 */

get_header(); ?>
	<div id="artigos" class="content">
		<ul>
			<?php
				$query = new WP_Query(array('post_type'=>'post'));
				if($query->have_posts()) : while($query->have_posts()) : $query->the_post();
					?>
						<li>
							<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
						</li>
					<?php
				endwhile; endif;
			?>
		</ul>
	</div>
<?php get_footer(); ?>