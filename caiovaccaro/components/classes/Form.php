<?php
abstract class Custom_form {

	public $name;
	public $context;
	public $post;
	public $input;
	public $post_type;
	public $value;
	public $inline;
	public $height;
	public $options;
	public $frontend_selected;
	public $compare;
	public $class;
	public $remove_label;

	public function __construct($args) {
		foreach ($args as $key => $value) {
			if(property_exists($this, $key)) {
				$this->$key = $value;
			}
		}
	}

	public function attr_name() {
		if($this->input) {
			echo $this->context.'_'.strtolower(remove_accents($this->input));
		} else {
			echo $this->context.'_'.strtolower(remove_accents($this->name));
		}
	}

	public function meta_name() {
		if($this->input) {
			return $this->context.'_'.strtolower(remove_accents($this->input));
		} else {
			return $this->context.'_'.strtolower(remove_accents($this->name));
		}
	}

	public function render_front($id) {
		return get_post_meta( $id, $this->meta_name(), true );
	}
}

class Input_text extends Custom_form {
	public function __construct($args) {
		parent::__construct($args);
	}

	public function render() {
		?>

		<label for="<? $this->attr_name(); ?>" class="custom-label-primary <? if($this->class) { echo $this->class; } ?>"><?= $this->name; ?>: </label>
		<input type="text" class="custom-input-primary <? if($this->class) { echo $this->class; } ?>" id="<? $this->attr_name(); ?>" name="<? $this->attr_name(); ?>" value="<?php echo esc_attr( get_post_meta( $this->post, $this->meta_name(), true ) ); ?>">

		<?
	}
}

class Input_textarea extends Custom_form {
	public function __construct($args) {
		parent::__construct($args);
	}

	public function render() {
		?>

		<label for="<? $this->attr_name(); ?>" class="custom-label-primary"><?= $this->name; ?>: </label>
		<textarea class="custom-input-primary" id="<? $this->attr_name(); ?>" name="<? $this->attr_name(); ?>"><?php echo preg_replace('#<br\s*?/?>#i', "\n", get_post_meta( $this->post, $this->meta_name(), true )); ?></textarea>

		<?
	}
}

class Input_checkbox extends Custom_form {
	public function __construct($args) {
		parent::__construct($args);
	}

	public function value() {
		echo $this->value;
	}

	public function meta_value() {
		return $this->value;
	}

	public function render() {

		$class = isset($this->class) ? $this->class : 'custom-checkbox-single-inline';

		?>

		<div class="<?= $class ?>">
			<input type="checkbox" id="<? $this->attr_name(); ?>" name="<? $this->attr_name(); ?>" value="<? $this->value(); ?>" <? check_meta_box_checkbox($this->post, $this->meta_name(), $this->meta_value()) ?>> <?= $this->name; ?>
		</div>
		<?
	}
}

class Input_multiple_checkbox extends Custom_form {
	public function __construct($args) {
		parent::__construct($args);
	}

	public function render() {
		$class = $this->inline == true ? 'custom-checkbox-inline' : 'custom-checkbox-block';

		?>

		<label for="<? $this->attr_name(); ?>" class="custom-label-primary"><?= $this->name; ?>: </label>
		
		<? if($this->inline != true): ?>
			<div class="custom-checkbox-loop-primary" <? if($this->height) echo 'style="height:'.$this->height.'"'; ?>>
		<? else: ?>

		<div class="custom-checkbox-loop-inline">

		<? endif;
		
		foreach ($this->options as $key => $value) { ?>
					
			<div class="<?= $class; ?>">
				<input name="<? $this->attr_name(); ?>[]" type="checkbox" value="<?= $key; ?>" <? check_meta_box_checkbox($this->post, $this->meta_name(), $key) ?>> <?= $value; ?>
			</div> <?

		} ?>
		</div>

		<?
	}
}

class Input_loop_checkbox extends Custom_form {
	public function __construct($args) {
		parent::__construct($args);
	}

	public function render() {
		$class = $this->inline == true ? 'custom-checkbox-inline' : 'custom-checkbox-block';

		?>

		<label for="<? $this->attr_name(); ?>" class="custom-label-primary"><?= $this->name; ?>: </label>
		
		<? if($this->inline != true): ?>
			<div class="custom-checkbox-loop-primary" <? if($this->height) echo 'style="height:'.$this->height.'"'; ?>>
		<? else: ?>

			<div class="custom-checkbox-loop-inline">

		<? endif;
		$args = array( 'post_type' => $this->post_type, 'nopaging' => true );
		$loop = new WP_Query( $args );

		if($loop->have_posts()) : while($loop->have_posts()) : $loop->the_post();
			$title = get_the_title();
			$value = str_replace('-','_',(str_replace('?'.$this->post_type.'=','',(basename(get_permalink()))))); ?>

			<div class="<?= $class; ?>">
				<input name="<? $this->attr_name(); ?>[]" type="checkbox" value="<?= $value; ?>" <? check_meta_box_checkbox($this->post, $this->meta_name(), $value) ?>> <?= $title; ?>
			</div>

		<? endwhile; wp_reset_postdata();  endif; ?>

		</div>

		<?
	}
}

class Input_loop_select extends Custom_form {
	public function __construct($args) {
		parent::__construct($args);
	}

	public function render() {

		$first_option = $this->remove_label == true ? $this->name : 'Escolha...';

		if($this->remove_label != true) :
		?>
		<label for="<? $this->attr_name(); ?>" class="custom-label-primary"><?= $this->name; ?>: </label>
		<? endif; ?>
		<div class="custom-select-loop">
			<select id="<? $this->attr_name(); ?>" name="<? $this->attr_name(); ?>">
			<option value=""><?= $first_option ?></option>
			<?
			$args = array( 'post_type' => $this->post_type, 'nopaging' => true );
			$loop = new WP_Query( $args );

			if($loop->have_posts()) : while($loop->have_posts()) : $loop->the_post();
				$title = get_the_title();
				$value = str_replace('-','_',(str_replace('?'.$this->post_type.'=','',(basename(get_permalink()))))); ?>
				
				<? if($this->frontend_selected == true) : ?>
					<option value="<?= $value; ?>" <? check_frontend_input('select', $value, $this->compare) ?>> <?= $title; ?> </option>
				<? else: ?>
					<option value="<?= $value; ?>" <? check_meta_box_select($this->post, $this->meta_name(), $value) ?>> <?= $title; ?> </option>
				<? endif; ?>

			<? endwhile; wp_reset_postdata();  endif; ?>

			</select>
		</div> <?
	}
}

class Input_select extends Custom_form {
	public function __construct($args) {
		parent::__construct($args);
	}

	public function render() {
		?>
		<label for="<? $this->attr_name(); ?>" class="custom-label-primary"><?= $this->name; ?>: </label>

		<div class="custom-select-single">

			<select id="<? $this->attr_name(); ?>" name="<? $this->attr_name(); ?>">
				<option value="">Escolha...</option>

				<? foreach ($this->options as $key => $value) { ?>
					
					<option value="<?= $key; ?>" <? check_meta_box_select($this->post, $this->meta_name(), $key) ?>> <?= $value; ?> </option>
					<?
				}
				
			?>
			</select>

		</div> <?
	}
}

class Input_loop_multiselect extends Custom_form {
	public function __construct($args) {
		parent::__construct($args);
	}

	public function render() {
		global $post;
		$original_post_id = get_the_ID();
		$original_post = $post;

		global $wpdb;

		$querystr = "
			SELECT DISTINCT wposts.*
			FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
			WHERE wposts.post_type = '$this->post_type'
			AND wposts.ID = wpostmeta.post_id
			AND wposts.post_status = 'publish'
		";

		$pageposts = $wpdb->get_results($querystr, OBJECT);

			if($pageposts): global $post;

			  ?> 
			  <div class="custom-multiselect">
			  	  <label for="<? $this->attr_name(); ?>" class="custom-label-primary"><?= $this->name; ?>: </label>
			  	  <div class="custom-select-loop">
			  		  <select name="<? $this->attr_name(); ?>[]" id="<? $this->attr_name(); ?>" multiple <? if($this->max_multiple) { echo 'max=<?= $this->max_multiple ?>'; } ?> class="chzn-select" style="width: 84%;" data-placeholder="Clique e escolha ou digite" tabindex="1"> <?

			  		  foreach ($pageposts as $post):

			  		    setup_postdata($post); 

			  			$value = str_replace('-','_',(str_replace('?'.$this->post_type.'=','',(basename(get_permalink())))));

			  			if($post->ID != $original_post_id) :

			  			?>

			  				<option value="<?= $value ?>" <? check_meta_box_multiple_select($original_post_id, $this->meta_name(), $value) ?>><?= the_title() ?></option>
			  		    	
			  		  <? endif;
			  		  endforeach;
			  		  ?> </select> 
			  	  </div>
			  </div>
			  <?
			else : ?>
			    <h4 class="center"><?= $this->name ?>: Nenhum conteúdo encontrado.</h4>

		<? endif;  wp_reset_postdata(); $post = $original_post;
	}
}

class Input_loop_select_option extends Custom_form {
	public function __construct($args) {
		parent::__construct($args);
	}

	public function render() {
		global $post;
		$original_post_id = get_the_ID();
		$original_post = $post;

		global $wpdb;

		$querystr = "
			SELECT DISTINCT wposts.*
			FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
			WHERE wposts.post_type = '$this->post_type'
			AND wposts.ID = wpostmeta.post_id
			AND wposts.post_status = 'publish'
		";

		$pageposts = $wpdb->get_results($querystr, OBJECT);

		if($pageposts): global $post;

			foreach ($pageposts as $post):

				setup_postdata($post); 

				$value = str_replace('-','_',(str_replace('?'.$this->post_type.'=','',(basename(get_permalink())))));

				if($post->ID != $original_post_id) :

				?>
					<option value="<?= $value ?>" <? check_option_multiple_select($this->meta_name(), $value, $this->index, $this->post, $this->plugin) ?>><?= the_title() ?></option>
					
				<?

				endif;
				
			endforeach;
			
		endif;  wp_reset_postdata(); $post = $original_post;
	}
}
?>