<?php get_header(); ?>
	<div id="primary" class="site-content">
		<div id="content" role="main">
			<article id="post-0" class="post error404 no-results not-found">
				<header class="entry-header">
					<h1 class="entry-title">Desculpe, não encontramos o que você está procurando.</h1>
				</header>
			</article>
		</div>
	</div>
<?php get_footer(); ?>