jQuery(function($){
	var CAIO = {};

	CAIO.Adm = {
		init: function() {
			if($('body').hasClass('wp-admin')) {
				$.each(CAIO.Adm, function(i,v) {
			        var func = i;
			        if(typeof CAIO.Adm[func] !== 'undefined' && func !== 'init' && typeof CAIO.Adm[func] === 'function') CAIO.Adm[func]();
			    });
			}
		}

		, chosen: function() {
			$.each($('.chzn-select'), function() {
				var max = $(this).attr('max');

				if(!$(this).hasClass('chzn-done') && $(this).is(':visible')) $(this).chosen({no_results_text: "Nenhum resultado encontrado"});

				if(typeof max !== 'undefined' && max !== false) {
					$(this).change(function() {
						var selected = $(this).find('option:selected').length;

						if(selected == max) {
							$(this).find('option:not(:selected)').prop('disabled', true);
							$(this).trigger('liszt:updated');
						} else {
							$(this).find('option').prop('disabled', false);
							$(this).trigger('liszt:updated');
						}
					});
				}
			});
		}

		, media_uploader: function() {

			if(typeof wp !== 'undefined') {
				var file_frame,
					post_type,
					wp_media_post_id = wp.media.model.settings.post.id,
					set_to_post_id,
					tr_template_source,
					tr_template,
					i,
					id;

				$('.wp_caio_table').on('click', '.CAIO_add_media_button', function(e) {
					set_to_post_id = $(this).closest('.inside').find('.media_post_id').val();
					post_type = $(this).closest('.inside').find('.media_post_type').val();
					i_array = $(this).attr('id').split('-');
					i = i_array[i_array.length - 1],
					id = post_type+'-' + i;

					console.log('set_to_post_id: '+set_to_post_id, 'post_type: '+post_type, 'i: '+i, 'id: '+id);

					e.preventDefault();

					if (file_frame) {
					  file_frame.uploader.uploader.param( 'post_id', set_to_post_id );
					  file_frame.open();

					  return;
					} else {
					  wp.media.model.settings.post.id = set_to_post_id;
					}

					file_frame = wp.media.frames.file_frame = wp.media({
					  title: $( this ).data( 'uploader_title' ),
					  button: {
					    text: $( this ).data( 'uploader_button_text' ),
					  },
					  multiple: false  
					});

					file_frame.on('select', function() {

					  var attachment = file_frame.state().get('selection').first().toJSON();
					  
					  $('#'+id).val(attachment.url).closest('tr').find('.button').text('Alterar imagem');
					  $('#'+post_type+'-imagem-'+i).attr('src', attachment.url).show();
					  
					  wp.media.model.settings.post.id = wp_media_post_id;

					  console.log('set_to_post_id: '+set_to_post_id, 'post_type: '+post_type, 'i: '+i, 'id: '+id);
					});

					file_frame.open();
				});


				$('a.add_media').on('click', function() {
					wp.media.model.settings.post.id = wp_media_post_id;

					console.log('set_to_post_id: '+set_to_post_id, 'post_type: '+post_type, 'i: '+i, 'id: '+id);
				});

				$('.adicionar_mais').click(function(e) {
					e.preventDefault();
					var post_type = $(this).closest('.inside').find('.media_post_type').val();

					var i = parseFloat($(this).closest('.inside').find('.wp_caio_table tr').last().find('.'+post_type).attr('id').replace(post_type+'-','')),
						x = i + 1,
						data = { 'id': x, 'i': i },
						tr_template_source = $('#'+post_type+'-template').html(),
						tr_template = Handlebars.compile(tr_template_source),
						result_template = tr_template(data);

					$(this).closest('.inside').find('.wp_caio_table tbody').append(result_template);
					CAIO.Adm.chosen();

					if($('#caio_destaques_slides').length) {
						$('#destaques_slides_number').val($('#caio_destaques_slides table tr').length);
					}
				});

				$('.wp_caio_table').on('click', '.remover-button', function() {
					var id_array = $(this).attr('id').split('-'),
						id = id_array[id_array.length - 1];

					if(id === '1' || $(this).hasClass('unique')) {
						$(this).closest('tr').find('input[type=text]').val('').end().find('input[type=hidden]').val('').end().find('textarea').val('').end().find('img').attr('src', '').hide().end().find('.button:contains("Alterar")').text('Adicionar imagem').end().find('select').val('').trigger('liszt:updated').end().find('select[multiple]').val('[]').end().trigger('liszt:updated').end().find('.link_destaque_home').find('.custom-multiselect').show().end().find('.link_texto').hide().end().find('.link_personalizado').removeAttr('checked').end().end().find('.tipo').find('input[checked]').click();

					} else {
						$(this).closest('tr').remove();
					}
				});
			}
		}

		, destaques: function() {
			if($('#caio_destaques_form').length) {
				$('#caio_destaques_slides').on('click', '.link_personalizado', function() {
					$(this).closest('td').find('.custom-multiselect').toggle().end().find('.link_texto').toggle();
				});

				if($('#caio_destaques_slides').length) {
					$(document).ready(function() {
						$('input[checked=checked]').click().attr('checked', 'checked');
					});
				}

				$('.tipo input').click(function() {
					if($(this).val() === 'personalizado') {
						$(this).closest('tr').find('.imagem, .upload_imagem, .info, .link').show().end().find('.conteudo_site, .video').hide();
					} else if($(this).val() === 'video') {	
						$(this).closest('tr').find('.imagem, .upload_imagem, .info, .link, .conteudo_site').hide().end().find('.video').show();
					} else if($(this).val() === 'site') {	
						$(this).closest('tr').find('.imagem, .upload_imagem, .info, .link, .video').hide().end().find('.conteudo_site').show();
						CAIO.Adm.chosen();
					}
				});

				$('#caio_destaques_save').click(function(e) {
					e.preventDefault();

					var empty_slide = [],
						empty_order = [];

					$.each($('.destaques_slides'), function() {
						if($(this).val() == '') {
							empty_slide.push('true');
						}
					});

					if($.inArray('true', empty_slide) !== -1) {
						$('#major-publishing-actions').find('p').remove();
						$('#major-publishing-actions').append('<p class="atencao">É obrigatório enviar imagens para os slides.</p>');
					}

					$.each($('.destaques_slides_ordem'), function() {
						if($(this).val() == '') {
							empty_order.push('true');
						}
					});

					if($.inArray('true', empty_order) !== -1) {
						$('#major-publishing-actions').find('p').remove();
						$('#major-publishing-actions').append('<p class="atencao">O campo ordem dos slides não pode ficar em branco.</p>');
					}

					if($.inArray('true', empty_slide) === -1 && $.inArray('true', empty_order) === -1) {
						$('#major-publishing-actions').find('.atencao').remove();
						$('#caio_destaques_form').submit();
					}

				});

				CAIO.Adm.Util.count_down('#destaques_info_content', '#destaques_info_content_count_down', 210);
			}
		}

		, parceiros: function() {
			if($('.exposicoes_parceiros').length) {
				var form = $('form[name="post"]');

				$(form).find('#publish').click(function(e) {
					var empty = [];

					$.each($('.exposicoes_parceiros'), function() {
						if($(this).val() == '') {
							empty.push('true');
						}
					});

					if($.inArray('true', empty) !== -1 && ($('#exposicoes_parceiros').find('tr').length === 1 && $('#exposicoes_parceiros').find('.exposicoes_parceiros_link').val() !== '')
						|| $.inArray('true', empty) !== -1 && $('#exposicoes_parceiros').find('tr').length > 1) {

						e.preventDefault();

						$('#major-publishing-actions').find('.atencao').remove();
						$('#major-publishing-actions').append('<p class="atencao">É obrigatório enviar imagens para os parceiros.</p>');
						setTimeout(function() {
							$('#publish').removeClass('button-primary-disabled');
							$('#publishing-action').find('.spinner').hide();
							$('#publish').removeClass('button-primary-disabled');
							$('#publishing-action').find('.spinner').hide();
						},100);
					} else {
						$('#major-publishing-actions').find('.atencao').remove();
					}
				});
			}

			if($('.dossies_parceiros').length) {
				var form = $('form[name="post"]');

				$(form).find('#publish').click(function(e) {
					var empty = [];

					$.each($('.dossies_parceiros'), function() {
						if($(this).val() == '') {
							empty.push('true');
						}
					});

					if($.inArray('true', empty) !== -1 && ($('#dossies_parceiros').find('tr').length === 1 && $('#dossies_parceiros').find('.dossies_parceiros_link').val() !== '')
						|| $.inArray('true', empty) !== -1 && $('#dossies_parceiros').find('tr').length > 1) {

						e.preventDefault();

						$('#major-publishing-actions').find('.atencao').remove();
						$('#major-publishing-actions').append('<p class="atencao">É obrigatório enviar imagens para os parceiros.</p>');
						setTimeout(function() {
							$('#publish').removeClass('button-primary-disabled');
							$('#publishing-action').find('.spinner').hide();
							$('#publish').removeClass('button-primary-disabled');
							$('#publishing-action').find('.spinner').hide();
						},100);
					} else {
						$('#major-publishing-actions').find('.atencao').remove();
					}
				});
			}
		}
	};

	CAIO.Adm.Util = {
		count_down: function(elem, countdown, max) {
		    function update() {
		    	var remaining = max - $(elem).val().length;
		    	
		    	$(countdown).text(remaining + ' caracteres restantes.');
		    }

		    update();

		    $(document).on('change, keyup', elem, function() {
		    	var new_val;

		    	update();

		    	if($(elem).val().length > max) {
		    		new_val = $(elem).val().slice(0,max);

		    		$(elem).val(new_val);
		    		update();
		    	}
		    });
		}
	};

	CAIO.Adm.init();
});