<?php
/**
 * Template Name: Contato
 */

get_header(); ?>

	<div id="contact" class="content">
		<form action="" id="contact_form">
			<p>
				<span class="myname">Oi Caio!</span>
				<br>Meu nome é <input type="text" name="name" id="name"> e email <input type="text" name="email" id="email"> .
				<br>Quero falar com você sobre <input type="text" name="subject" id="subject"> .
			</p>
			<textarea name="message" id="message"></textarea>
			<div class="bottom">
				<button id="enviar">
					<i id="truck" class="ss-icon ss-pika">deliveryvan</i> <span>enviar</span>
				</button>
				<div class="thanks">
					<p>Obrigado!</p>
				</div>
				<div class="validation">
					<p>Ops! Esqueceu alguma coisa?</p>
				</div>
			</div>
		</form>
	</div>
<?php get_footer(); ?>