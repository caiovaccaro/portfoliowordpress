<?php
function check_meta_box_select($post, $meta, $value) {
	$compare = get_post_meta( $post, $meta, true );

	if($value == $compare) {
		echo 'selected="selected"';
	}
}

function check_meta_box_multiple_select($post, $meta, $value) {
	$compare = get_post_meta( $post, $meta, true );

	if(strpos($compare, ', ')) {
		$array = explode(', ', $compare);

		foreach ($array as $select) {
			if($value == $select) {
				echo 'selected="selected"';
			}
		}
	} else {
		if($value == $compare) {
			echo 'selected="selected"';
		}
	}
}

function check_option_multiple_select($meta, $value, $i, $post, $plugin) {
	if($plugin) {
		$compare = get_option($meta);
	} else {
		$compare = get_post_meta( $post, $meta, true );		
	}

	if($i) {
		$compare = $compare[$i];
	}

	if(is_array($compare)) {
		foreach ($compare as $select) {
			if($value == $select) {
				echo 'selected="selected"';
			}
		}
	} elseif(strpos($compare, ', ')) {
		$array = explode(', ', $compare);

		foreach ($array as $select) {
			if($value == $select) {
				echo 'selected="selected"';
			}
		}
	} else {
		if($value == $compare) {
			echo 'selected="selected"';
		}
	}
}

function check_meta_box_checkbox($post, $meta, $value) {
	$compare = get_post_meta( $post, $meta, true );

	if(strpos($compare, ', ')) {
		$array = explode(', ', $compare);

		foreach ($array as $checkbox) {
			if($value == $checkbox) {
				echo 'checked="checked"';
			}
		}
	} else {
		if($value == $compare) {
			echo 'checked="checked"';
		}
	}
}

function check_frontend_input($type, $value, $compare) {
	$text = $type == 'checkbox' ? 'checked="checked"' : 'selected="selected"';

	if($value == $compare) {
		echo $text;
	}
}
?>